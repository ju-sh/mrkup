v0.2 
----
* rename `stringify()` methods to `dumps()`
* add `dump()` methods

v0.1 (2020-Dec-08)
----
First version
